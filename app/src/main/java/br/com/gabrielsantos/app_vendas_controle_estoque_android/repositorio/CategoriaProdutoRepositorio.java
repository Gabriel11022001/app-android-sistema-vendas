package br.com.gabrielsantos.app_vendas_controle_estoque_android.repositorio;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;
import java.util.List;

import br.com.gabrielsantos.app_vendas_controle_estoque_android.model.CategoriaProduto;

public class CategoriaProdutoRepositorio {

    private SQLiteDatabase bancoDados;

    public CategoriaProdutoRepositorio(Context contextoApp) throws SQLiteException {
        this.bancoDados = contextoApp.openOrCreateDatabase("db_app_android_controle_estoque_e_vendas", Context.MODE_PRIVATE, null);
    }

    @SuppressLint("Range")
    public List<CategoriaProduto> buscarTodasCategorias() {

        try {
            String query = "SELECT * FROM tbl_categorias;";
            Cursor cursor = this.bancoDados.rawQuery(query, null);
            List<CategoriaProduto> categorias = new ArrayList<>();

            if (cursor != null) {

                while (cursor.moveToNext()) {
                    CategoriaProduto categoriaProduto = new CategoriaProduto();
                    categoriaProduto.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    categoriaProduto.setDescricao(cursor.getString(cursor.getColumnIndex("descricao")));
                    boolean status = cursor.getInt(cursor.getColumnIndex("status")) == 1;
                    categoriaProduto.setStatus(status);
                    categorias.add(categoriaProduto);
                }

            }

            return categorias;
        } catch (Exception e) {

            return null;
        }

    }

    public boolean cadastrarCategoriaDeProduto(CategoriaProduto categoriaProduto) throws Exception {
        ContentValues contentValues = new ContentValues();
        contentValues.put("descricao", categoriaProduto.getDescricao());
        contentValues.put("status", categoriaProduto.getStatus() ? 1 : 0);
        this.bancoDados.insertOrThrow("tbl_categorias", null, contentValues);

        return true;
    }
}
