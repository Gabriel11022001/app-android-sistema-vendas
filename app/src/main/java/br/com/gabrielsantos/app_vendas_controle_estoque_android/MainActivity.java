package br.com.gabrielsantos.app_vendas_controle_estoque_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import br.com.gabrielsantos.app_vendas_controle_estoque_android.utils.CriaBancoDados;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context contextoApp = getApplicationContext();
        // criando o banco de dados e as tabelas caso as tabelas não existam
        CriaBancoDados criaBancoDados = new CriaBancoDados(contextoApp);
        criaBancoDados.criarBancoDadosETabelas();
        // configurando a splash screen
        this.configurarSplashScreen();
    }

    private void configurarSplashScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        }, 4000);
    }
}