package br.com.gabrielsantos.app_vendas_controle_estoque_android;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.zip.Inflater;

import br.com.gabrielsantos.app_vendas_controle_estoque_android.model.Usuario;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.repositorio.UsuarioRepositorio;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private UsuarioRepositorio usuarioRepositorio;
    private ViewHolder viewHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.usuarioRepositorio = new UsuarioRepositorio(getApplicationContext());
        this.viewHolder = new ViewHolder();
        // mapeando os elementos
        this.viewHolder.buttonRealizarLogin = findViewById(R.id.btn_efetuar_login);
        this.viewHolder.editTextLogin = findViewById(R.id.txt_login);
        this.viewHolder.editTextSenha = findViewById(R.id.txt_senha);
        // mapeando as ações
        this.viewHolder.buttonRealizarLogin.setOnClickListener(this);

        // removendo a action bar
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

    }

    @Override
    public void onClick(View view) {

        String resultadoLogin = "";
        Usuario usuarioLogado = null;

        try {

            if (view.getId() == R.id.btn_efetuar_login) {
                // clicou no botão de login
                // buscar usuário pelo login e senha
                String login = this.viewHolder.editTextLogin.getText().toString();
                String senha = this.viewHolder.editTextSenha.getText().toString();

                // validando o login e a senha
                if (login.equals("") || senha.equals("")) {
                    resultadoLogin = "Informe o login e a senha!";
                } else {
                    usuarioLogado = this.usuarioRepositorio.buscarUsuarioPeloLoginSenha(login, senha);

                    if (usuarioLogado != null) {
                        /**
                         * Encontrou usuário com login e senha informados,
                         */
                        resultadoLogin = "Usuário logado com sucesso!";
                    } else {
                        // login ou senha inválidos
                        resultadoLogin = "Login ou senha inválidos!";
                    }
                }

            }

        } catch (Exception e) {
            resultadoLogin = "Ocorreu o seguinte erro: " + e.getMessage();
        }

        if (resultadoLogin.equals("Informe o login e a senha!") == false) {
            // apresenta carregamento da tela
            
        }

        if (resultadoLogin.equals("Usuário logado com sucesso!")) {
            // usuário foi logado com sucesso!
            Bundle bundle = new Bundle();
            bundle.putInt("id_usuario_logado", usuarioLogado.getId());
            bundle.putString("nome_usuario_logado", usuarioLogado.getNome());
            bundle.putString("nivel_acesso_usuario_logado", usuarioLogado.getNivelAcesso());
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        } else if (resultadoLogin.equals("Informe o login e a senha!")) {
            Toast.makeText(this, resultadoLogin, Toast.LENGTH_LONG).show();
        } else {
            this.apresentarToastComResultadoLogin(resultadoLogin);
        }

    }

    private void apresentarToastComResultadoLogin(String mensagem) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // após 3.3 segundos apresenta o Toast
                Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_LONG).show();
            }
        }, 3300);
    }

    private static class ViewHolder {

        public Button buttonRealizarLogin;
        public EditText editTextLogin;
        public EditText editTextSenha;
    }
}