package br.com.gabrielsantos.app_vendas_controle_estoque_android.eventos;

import br.com.gabrielsantos.app_vendas_controle_estoque_android.model.CategoriaProduto;

public interface IOnClickCategoriaProdutoItem {

    void onClick(CategoriaProduto categoriaProduto);
}
