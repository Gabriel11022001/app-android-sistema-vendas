package br.com.gabrielsantos.app_vendas_controle_estoque_android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gabrielsantos.app_vendas_controle_estoque_android.R;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.adapter_viewholder.CategoriaProdutoViewHolder;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.eventos.IOnClickCategoriaProdutoItem;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.model.CategoriaProduto;

public class CategoriaProdutoAdapter extends RecyclerView.Adapter<CategoriaProdutoViewHolder> {

    private List<CategoriaProduto> categoriasDeProduto;
    private IOnClickCategoriaProdutoItem onClickCategoriaProdutoItem;

    public CategoriaProdutoAdapter(List<CategoriaProduto> categoriasDeProduto, IOnClickCategoriaProdutoItem onClickCategoriaProdutoItem) {
        this.categoriasDeProduto = categoriasDeProduto;
        this.onClickCategoriaProdutoItem = onClickCategoriaProdutoItem;
    }

    @NonNull
    @Override
    public CategoriaProdutoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext(); // obtendo o contexto
        LayoutInflater layoutInflater = LayoutInflater.from(context); // definindo o inflater
        View view = layoutInflater.inflate(R.layout.categoria_produto_item, parent, false);

        return new CategoriaProdutoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriaProdutoViewHolder holder, int position) {
        CategoriaProduto categoriaProdutoAtual = this.categoriasDeProduto.get(position);
        holder.definirElementos(categoriaProdutoAtual, this.onClickCategoriaProdutoItem);
    }

    @Override
    public int getItemCount() {

        return this.categoriasDeProduto.size();
    }
}
