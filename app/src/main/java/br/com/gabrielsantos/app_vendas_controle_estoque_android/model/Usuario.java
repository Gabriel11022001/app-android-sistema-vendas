package br.com.gabrielsantos.app_vendas_controle_estoque_android.model;

public class Usuario {

    private int id;
    private String login;
    private String senha;
    private String nome;
    private String nivelAcesso;
    private boolean status;

    public Usuario() {}

    public Usuario(int id, String nome, String login, String senha, String nivelAcesso, boolean status) {
        this.setId(id);
        this.setNome(nome);
        this.setLogin(login);
        this.setSenha(senha);
        this.setNivelAcesso(nivelAcesso);
        this.setStatus(status);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {

        return this.id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {

        return this.nome;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {

        return this.login;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSenha() {

        return this.senha;
    }

    public void setNivelAcesso(String nivelAcesso) {
        this.nivelAcesso = nivelAcesso;
    }

    public String getNivelAcesso() {

        return this.nivelAcesso;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {

        return this.status;
    }
}
