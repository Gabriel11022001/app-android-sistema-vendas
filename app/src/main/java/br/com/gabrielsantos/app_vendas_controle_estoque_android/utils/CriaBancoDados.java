package br.com.gabrielsantos.app_vendas_controle_estoque_android.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CriaBancoDados {

    private Context contextoAplicacao;

    public CriaBancoDados(Context contextoAplicacao) {
        this.contextoAplicacao = contextoAplicacao;
    }

    public void criarBancoDadosETabelas() {

        try {
            // criando o banco de dados
            SQLiteDatabase bancoDados = this.contextoAplicacao.openOrCreateDatabase("db_app_android_controle_estoque_e_vendas", Context.MODE_PRIVATE, null);
            // criando a tabela de usuários
            // bancoDados.execSQL("DROP TABLE tbl_usuarios;");
            String queryCriarTabelaUsuarios = "CREATE TABLE IF NOT EXISTS tbl_usuarios(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "nome VARCHAR(255) NOT NULL," +
                    "login VARCHAR(255) NOT NULL," +
                    "senha VARCHAR(255) NOT NULL," +
                    "nivel_acesso VARCHAR(255) NOT NULL," +
                    "status BOOLEAN NOT NULL DEFAULT 1);";
            bancoDados.execSQL(queryCriarTabelaUsuarios);
            // criando tabela de categorias de produtos
            String queryCriarTabelaCategorias = "CREATE TABLE IF NOT EXISTS tbl_categorias(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "descricao TEXT NOT NULL," +
                    "status BOOLEAN NOT NULL DEFAULT 1);";
            bancoDados.execSQL(queryCriarTabelaUsuarios);
            bancoDados.execSQL(queryCriarTabelaCategorias);
            // cadastrando alguns usuários padrões no banco de dados na tabela tbl_usuarios
            bancoDados.execSQL("INSERT INTO tbl_usuarios(nome, login, senha, nivel_acesso, status)" +
                    "VALUES('Gabriel', 'gabriel123', 'gabriel123', 'admin', 1);");
            bancoDados.execSQL("INSERT INTO tbl_usuarios(nome, login, senha, nivel_acesso, status)" +
                    "VALUES('Pedro', 'pedro123', 'pedro123', 'admin', 1);");
            bancoDados.execSQL("INSERT INTO tbl_usuarios(nome, login, senha, nivel_acesso, status)" +
                    "VALUES('Fernando', 'fernando123', 'fernando123', 'admin', 1);");
            // cadastrando algumas categorias de produtos
            bancoDados.execSQL("INSERT INTO tbl_categorias(descricao, status)" +
                    "VALUES('Categoria qualquer', 1);");
            bancoDados.execSQL("INSERT INTO tbl_categorias(descricao, status)" +
                    "VALUES('Categoria qualquer 2', 1);");
            bancoDados.execSQL("INSERT INTO tbl_categorias(descricao, status)" +
                    "VALUES('Categoria qualquer 3', 1);");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
