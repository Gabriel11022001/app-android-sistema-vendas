package br.com.gabrielsantos.app_vendas_controle_estoque_android.repositorio;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;
import java.util.List;

import br.com.gabrielsantos.app_vendas_controle_estoque_android.model.Usuario;

public class UsuarioRepositorio {

    private SQLiteDatabase bancoDados;

    public UsuarioRepositorio(Context contextoApp) throws SQLiteException {
        // iniciando conexão com o banco de dados
        this.bancoDados = contextoApp.openOrCreateDatabase("db_app_android_controle_estoque_e_vendas", Context.MODE_PRIVATE, null);
    }

    public String cadastrarUsuarioBancoDados(Usuario usuario) {

        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("nome", usuario.getNome());
            contentValues.put("login", usuario.getLogin());
            contentValues.put("senha", usuario.getSenha());
            contentValues.put("nivel_acesso", usuario.getNivelAcesso());
            contentValues.put("status", (usuario.getStatus() ? 1 : 0));
            this.bancoDados.insertOrThrow("tbl_usuarios", null, contentValues);

            return "Usuário cadastrado com sucesso!";
        } catch (SQLException e) {
            // e.printStackTrace();

            return "Ocorreu o seguinte erro ao tentar-se cadastrar o usuário: " + e.getMessage();
        }

    }

    @SuppressLint("Range")
    public List<Usuario> buscarTodosUsuarios() {
        List<Usuario> usuarios;

        try {
            String query = "SELECT * FROM tbl_usuarios;";
            Cursor cursor = this.bancoDados.rawQuery(query, null);
            usuarios = new ArrayList<>();

            if (cursor != null) {

                while (cursor.moveToNext()) {
                    Usuario usuario = new Usuario();
                    usuario.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    usuario.setNome(cursor.getString(cursor.getColumnIndex("nome")));
                    usuario.setLogin(cursor.getString(cursor.getColumnIndex("login")));
                    usuario.setSenha(cursor.getString(cursor.getColumnIndex("senha")));
                    usuario.setNivelAcesso(cursor.getString(cursor.getColumnIndex("nivel_acesso")));
                    int statusInteiro = cursor.getInt(cursor.getColumnIndex("status"));
                    boolean statusBooleano = statusInteiro == 1;
                    usuario.setStatus(statusBooleano);
                    usuarios.add(usuario);
                }

            }

        } catch (Exception e) {
            usuarios = new ArrayList<>();
        }

        return usuarios;
    }

    @SuppressLint("Range")
    public Usuario buscarUsuarioPeloLoginSenha(String login, String senha) {

        try {
            String query = "SELECT * FROM tbl_usuarios WHERE login = '" + login + "'" +
                    "AND senha = '" + senha +"';";
            Cursor cursor = this.bancoDados.rawQuery(query, null);

            if (cursor.moveToNext()) {

                if (cursor.getString(cursor.getColumnIndex("login")).equals(login)
                        && cursor.getString(cursor.getColumnIndex("senha")).equals(senha)) {
                    Usuario usuario = new Usuario();
                    usuario.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    usuario.setNome(cursor.getString(cursor.getColumnIndex("nome")));
                    usuario.setLogin(cursor.getString(cursor.getColumnIndex("login")));
                    usuario.setSenha(cursor.getString(cursor.getColumnIndex("senha")));
                    usuario.setNivelAcesso(cursor.getString(cursor.getColumnIndex("nivel_acesso")));
                    int statusInteiro = cursor.getInt(cursor.getColumnIndex("status"));
                    boolean statusBooleano = statusInteiro == 1;
                    usuario.setStatus(statusBooleano);

                    return usuario;
                }

            }

            return null;
        } catch (Exception e) {

            return null;
        }

    }
}
