package br.com.gabrielsantos.app_vendas_controle_estoque_android.ui.categoria_produto;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import br.com.gabrielsantos.app_vendas_controle_estoque_android.CadastrarCategoriaProdutoActivity;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.R;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.adapter.CategoriaProdutoAdapter;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.databinding.FragmentCategoriaProdutoBinding;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.eventos.IOnClickCategoriaProdutoItem;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.model.CategoriaProduto;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.repositorio.CategoriaProdutoRepositorio;

public class CategoriaProdutoFragment extends Fragment {

    private FragmentCategoriaProdutoBinding fragmentCategoriaProdutoBinding;
    private RecyclerView recyclerViewCategoriasDeProduto;
    private TextView textMensagem;
    private CategoriaProdutoRepositorio categoriaProdutoRepositorio;
    private FloatingActionButton buttonCadastrarCategoriaProduto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_categoria_produto, container, false);
        this.recyclerViewCategoriasDeProduto = view.findViewById(R.id.recycler_categorias);
        this.buttonCadastrarCategoriaProduto = view.findViewById(R.id.button_cadastrar_categoria);
        this.recyclerViewCategoriasDeProduto.setVisibility(View.GONE);
        this.textMensagem = view.findViewById(R.id.text_mensagem);
        this.textMensagem.setVisibility(View.GONE);
        // buscando todas as categorias cadastradas no banco de dados
        this.categoriaProdutoRepositorio = new CategoriaProdutoRepositorio(getContext());
        List<CategoriaProduto> categoriaProdutos = this.categoriaProdutoRepositorio.buscarTodasCategorias();

        if (categoriaProdutos == null || categoriaProdutos.size() == 0) {
            this.definirPaddingNaTela(view);
        }

        if (categoriaProdutos == null) {
            // ocorreu uma exceção no momento de buscar todas as categorias cadastradas
            this.textMensagem.setVisibility(View.VISIBLE);
            this.textMensagem.setText("Ocorreu um erro ao tentar-se consultar as categorias cadastradas no banco de dados!");
            this.textMensagem.setTextColor(Color.RED);
        } else if (categoriaProdutos.size() == 0) {
            // não existem categorias cadastradas no banco de dados
            this.textMensagem.setVisibility(View.VISIBLE);
            this.textMensagem.setText("Não existem categorias cadastradas no banco de dados...");
        } else {
            this.recyclerViewCategoriasDeProduto.setVisibility(View.VISIBLE);
            // definindo o evento de click sobre o item redirecionando para a tela de detalhes
            IOnClickCategoriaProdutoItem onClickCategoriaProdutoItem = new IOnClickCategoriaProdutoItem() {
                @Override
                public void onClick(CategoriaProduto categoriaProduto) {
                    View viewTelaDetalhesCategoriaProduto = getLayoutInflater().inflate(R.layout.modal_detalhes_categoria_produto, null);
                    TextView textDetalhesCategoriaId = viewTelaDetalhesCategoriaProduto.findViewById(R.id.text_id_categoria);
                    TextView textDetalhesCategoriaDescricao = viewTelaDetalhesCategoriaProduto.findViewById(R.id.text_descricao_categoria);
                    TextView textDetalhesCategoriaStatus = viewTelaDetalhesCategoriaProduto.findViewById(R.id.text_status_categoria);
                    Button buttonFecharModal = viewTelaDetalhesCategoriaProduto.findViewById(R.id.button_fechar_modal);
                    textDetalhesCategoriaId.setText("Código da categoria: #" + categoriaProduto.getId());
                    textDetalhesCategoriaDescricao.setText("Descrição da categoria: " + categoriaProduto.getDescricao());
                    String status = "";

                    if (categoriaProduto.getStatus()) {
                        status = "Ativo";
                        textDetalhesCategoriaStatus.setTextColor(Color.parseColor("#10ac84"));
                    } else {
                        status = "Inativo";
                        textDetalhesCategoriaStatus.setTextColor(Color.RED);
                    }

                    textDetalhesCategoriaStatus.setText("Status da categoria: " + status);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setView(viewTelaDetalhesCategoriaProduto);
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    buttonFecharModal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                }
            };
            CategoriaProdutoAdapter categoriaProdutoAdapter = new CategoriaProdutoAdapter(categoriaProdutos, onClickCategoriaProdutoItem);
            this.recyclerViewCategoriasDeProduto.setLayoutManager(new LinearLayoutManager(getContext()));
            this.recyclerViewCategoriasDeProduto.setAdapter(categoriaProdutoAdapter);
        }

        this.redirecionarUsuarioTelaCadastroCategoriaProduto();

        return view;
    }

    private void definirPaddingNaTela(View view) {
        view.setPadding(15, 15, 15, 15);
    }

    private void redirecionarUsuarioTelaCadastroCategoriaProduto() {
        this.buttonCadastrarCategoriaProduto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CadastrarCategoriaProdutoActivity.class);
                startActivity(intent);
            }
        });
    }
}