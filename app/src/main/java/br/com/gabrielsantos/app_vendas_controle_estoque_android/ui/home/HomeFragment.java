package br.com.gabrielsantos.app_vendas_controle_estoque_android.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import br.com.gabrielsantos.app_vendas_controle_estoque_android.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding fragmentHomeBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false);

        return this.fragmentHomeBinding.getRoot();
    }
}