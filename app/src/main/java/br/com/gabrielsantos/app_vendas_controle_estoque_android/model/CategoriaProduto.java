package br.com.gabrielsantos.app_vendas_controle_estoque_android.model;

public class CategoriaProduto {

    private int id;
    private String descricao;
    private boolean status;

    public CategoriaProduto() {}

    public CategoriaProduto(int id, String descricao, boolean status) {
        this.setId(id);
        this.setDescricao(descricao);
        this.setStatus(status);
    }

    public int getId() {

        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {

        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean getStatus() {

        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
