package br.com.gabrielsantos.app_vendas_controle_estoque_android;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import br.com.gabrielsantos.app_vendas_controle_estoque_android.adapter.CategoriaProdutoAdapter;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.model.CategoriaProduto;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.repositorio.CategoriaProdutoRepositorio;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.ui.categoria_produto.CategoriaProdutoFragment;

public class CadastrarCategoriaProdutoActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editDescricaoCategoria;
    private RadioButton radioAtivo;
    private RadioButton radioInativo;
    private TextView textMensagemInformeDescricaoCategoria;
    private Button buttonCadastrarCategoria;
    private CategoriaProdutoRepositorio categoriaProdutoRepositorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_categoria_produto);

        // realizando conexão com o banco de dados
        try {
            this.categoriaProdutoRepositorio = new CategoriaProdutoRepositorio(this);
        } catch (Exception e) {
            Log.e("ERRO", "Ocorreu o seguinte erro ao tentar-se conectar ao banco de dados: " + e.getMessage());
        }

        // mapeando os elementos
        this.buttonCadastrarCategoria = findViewById(R.id.button_cadastrar_categoria);
        this.textMensagemInformeDescricaoCategoria = findViewById(R.id.text_mensagem_informe_descricao);
        this.editDescricaoCategoria = findViewById(R.id.edit_descricao);
        this.radioAtivo = findViewById(R.id.radio_ativo);
        this.radioInativo = findViewById(R.id.radio_inativo);
        this.textMensagemInformeDescricaoCategoria.setVisibility(View.GONE);
        this.radioAtivo.setChecked(true);
        this.configurarActionBar();
        this.buttonCadastrarCategoria.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        // fechando a tela de cadastro de categoria e voltando para a listagem de categorias
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return true;
    }

    private void configurarActionBar() {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    private void cadastrarCategoriaDeProduto() {

        if (this.categoriaProdutoRepositorio != null) {

            try {
                String descricao = this.editDescricaoCategoria.getText().toString();
                boolean status = false;

                // validando a descrição da categoria
                if (descricao.equals("")) {
                    this.textMensagemInformeDescricaoCategoria.setVisibility(View.VISIBLE);

                    return;
                }

                this.textMensagemInformeDescricaoCategoria.setVisibility(View.GONE);

                if (this.radioAtivo.isChecked()) {
                    status = true;
                }

                CategoriaProduto categoriaProduto = new CategoriaProduto();
                categoriaProduto.setDescricao(descricao);
                categoriaProduto.setStatus(status);
                this.categoriaProdutoRepositorio.cadastrarCategoriaDeProduto(categoriaProduto);
                this.limparCampos();
                Toast.makeText(getApplicationContext(), "Categoria cadastrada com sucesso!", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Log.e("ERRO", "Ocorreu o seguinte erro ao tentar-se cadastrar a categoria: " + e.getMessage());
            }

        }

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.button_cadastrar_categoria) {
            // cadastrar categoria de produto
            this.cadastrarCategoriaDeProduto();
        }

    }

    private void limparCampos() {
        this.editDescricaoCategoria.setText("");
    }
}