package br.com.gabrielsantos.app_vendas_controle_estoque_android.adapter_viewholder;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.gabrielsantos.app_vendas_controle_estoque_android.CadastrarCategoriaProdutoActivity;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.R;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.eventos.IOnClickCategoriaProdutoItem;
import br.com.gabrielsantos.app_vendas_controle_estoque_android.model.CategoriaProduto;

public class CategoriaProdutoViewHolder extends RecyclerView.ViewHolder {

    private TextView textCodigoCategoria;
    private TextView textDescricaoCategoria;
    private LinearLayout categoriaProdutoItem;
    private CategoriaProduto categoriaProduto;
    private IOnClickCategoriaProdutoItem onClickCategoriaProdutoItem;

    public CategoriaProdutoViewHolder(@NonNull View itemView) {
        super(itemView);
        this.textCodigoCategoria = itemView.findViewById(R.id.text_codigo_categoria);
        this.textDescricaoCategoria = itemView.findViewById(R.id.text_descricao_categoria);
        this.categoriaProdutoItem = itemView.findViewById(R.id.categoria_produto_item);
        // definindo o evento de click sobre o item
        this.categoriaProdutoItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickCategoriaProdutoItem.onClick(categoriaProduto);
            }
        });
    }

    public void definirElementos(CategoriaProduto categoriaProduto, IOnClickCategoriaProdutoItem onClickCategoriaProdutoItem) {
        // Log.i("CATEGORIA", categoriaProduto.getDescricao());
        this.categoriaProduto = categoriaProduto;
        this.onClickCategoriaProdutoItem = onClickCategoriaProdutoItem;
        this.textCodigoCategoria.setText("#" + this.categoriaProduto.getId());
        this.textDescricaoCategoria.setText(this.categoriaProduto.getDescricao());
    }
}
